const helloController = require('../controllers/helloController');
const userController = require('../controllers/usersController');
const transactionController = require('../controllers/transactionController');
const messageController = require('../controllers/messageController');
const fundController = require('../controllers/fundController');
const jwtCheck = require('../middleware/jwcheck');

module.exports = function(app) {
	app.route('/').get(helloController.hello);

	app.route('/login/').post(userController.User_login); //authentification
	app
		.route('/user')
		.get(jwtCheck.verify_token, userController.User_infos) //récupération des données de l'utilisateur
		.post(userController.User_register) //création d'un compte
		.put(jwtCheck.verify_token, userController.User_modifier) //modifier les données de l'utilisateur
		.delete(jwtCheck.verify_token, userController.User_delete); //suppression des données de l'utilisateur

	app
		.route('/transaction/')
		.get(transactionController.list_all_tx) // Lister toutes les transactions
		.post(transactionController.create_a_tx); //Ajout d'une transaction EUR to TOKEN or TOKEN to SHARE

	app.route('/usertxs')
	   .get(transactionController.list_userTxs); // Récupération de toutes les tx d'un utilisateur

	app.route('/message')
	   .get(messageController.Message_fetch) // Récupération de tous les messages destiné à un user
	   .post(messageController.Message_new); // Envoi d'un message d'un user à un autre

	app
		.route('/transaction/:ownerId')
		.get(transactionController.list_some_tx); // Lister les Tx de l'user courant

	app
		.route('/fund')
		.get(fundController.list_fund_data) //récupérer les informations du fonds
		.post(fundController.init_fund) //initialiser les informations du fonds
		.put(fundController.update_fund_data); //mettre à jour les données du fonds

	app.route('/exchange/stats').get(); //information générales du fonds

	
};
