var mongoose = require('mongoose');

/*
 * sender_id : id user auto mongo
 * target_id : id user auto mongo
 * tx_ids : tableau d'id des tx partagées
 * dateSend : comme son nom l'indique
 * 
*/

var MessageSchema = new mongoose.Schema({
    sender_id: String,
    target_id: String,
    tx_ids: [String],
    dateSend: { type: Date, default: Date.now },
    readed: { type: Boolean, default: false }
});

module.exports = mongoose.model('Message', MessageSchema);