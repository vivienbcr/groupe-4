var mongoose = require('mongoose');

var FundSchema = new mongoose.Schema({
    name: String,
    share_owned: Number,
    token_per_share: Number,
    share_type: String,
    share_per_type: String
});

module.exports = mongoose.model('Fund', FundSchema);