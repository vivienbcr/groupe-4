
# [HEDGEFUND] Api

## Setup

1. Cloner ou se baser sur la branche develop
2. Installer les dépendances

    ```bash
    ~/groupe-4/api$  npm install
    ou
    ~/groupe-4/api$  yarn install
    ```

3. Lancer l'application

Les différents modes de l'application nécessitent une base de donnée mongodb disponible sur le port par défault :::27017. Si vous démarrez le container docker en même temps que l'instance en mode devloppement, l'application se connecteras à la base de donnée du container.

- En mode production : Application disponible [localhost:3000](http://localhost:3000)

    ```bash
    ~/groupe-4/api$  npm run server
    ou
    ~/groupe-4/api$  yarn server
    ```

- En mode devloppement : Application disponible [localhost:3333](http://localhost:3333)

    ```bash
    ~/groupe-4/api$  npm run dev
    ou
    ~/groupe-4/api$  yarn dev
    ```

- En mode Container Docker : Application disponible [localhost:3000](http://localhost:3000)

    ```bash
    # Start
    ~/groupe-4$ docker-compose up --build
    # Stop
    ~/groupe-4$ docker-compose down
    ```
