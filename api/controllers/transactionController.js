const Transaction = require('../models/transactionModel.js');

/*
 * Data à recevoir :
 * owner_id : Id de l'utilisateur connecté
 * from_addr : Adresse sur le SC de l'utilisateur connecté 
 * src : Source (indicateur envoyé par le formulaire) : tokenTx ou shareTx
 * tx_type : Type (indicateur envoyé par le formulaire) : achat ou vente
 * target : Cible (indicateur envoyé par le formulaire) : SC_Token ou Fund_Name (n'importe)
 * Montants (fiat, token, share, dépend du type de tx et de la source)
*/

exports.Transaction_Save = function(req, res) {
    try{
        // Controle eventuellement a revoir en fonction du submit du formulaire d'achat/vente
        if((req.body.token_amount && req.body.src == 'tokenTx') || (req.body.share_amount && req.body.src == 'shareTx')){

            // TODO : Récupération de l'ID de l'owner (module $session ?)
            const owner_id = req.body.owner_id;
            const currDate = new Date();

            // Addresse de l'utilisateur sur le smart contract
            // Idéalement en variable de session comme pour l'ID
            const from_addr = "0xb9275A4dd4D2533eBdFc48e7109Be79c54188DF7";

            // Indicateur permettant de savoir depuis quel type de formulaire d'achat
            // est réalisé la transaction 
            //  ( Fiat  --> Token )
            //          OU
            //  ( Token --> Share )
            const src = req.body.src;

            // tx_type représente le type de transaction (achat ou vente)
            let tx_type;

            // Target représente la cible : peut être un fond d'investissement ou SC_Token
            let target;

            // From_addr représente la source : peut être SC_Token ou Bank_Account
            const fiat = null;
            const token = null;
            const share = null;

            if(src == 'tokenTx' && req.body.token_amount){
                tx_type = "buy";
                target = "buyToken";
                from_addr = "bank_account";
                token = req.body.token_amount;
                // Parité 1/1 - 1token = 1euro/dollar
                fiat = token;
            }else if(src == 'shareTx' && req.body.share_amount){
                //tx_type = 
                if(req.body.type_tx == 'buy') {
                    target = "buyShare";
                } else if(req.body.type_tx == 'sell') {
                    target = "sellShare";
                }
                // from_addr déjà définis avec adresse du SC
                share = req.body.share_amount;
                token = share * 100;
            }

            let new_transaction = new Transaction();
            new_transaction.owner_id = owner_id;
            new_transaction.dateCrea = currDate;
            new_transaction.target = target;
            new_transaction.from_addr = from_addr;
            new_transaction.fiat = fiat;
            new_transaction.token = token;
            new_transaction.share = share;
            new_transaction.save(function(err, transaction){
                if (err) { 
                    res.status(400).send(err); 
                } else {
                    res.status(201).json(transaction);
                }
            });
        } else {
            res.status(400).send({ err: 'Bad request' });
            console.log("transactionController : Transaction_save - Origine non reconnue ou données manquantes")
        }
    } catch (error) {
        console.log('transactionController : Transaction_save error', error);
		res.status(500).send('Internal server error');
    }
};

exports.list_userTxs = async function(req, res) {
    try{
        console.log("Req : ", req.query);
        if(req.query.id.length > 0){
            Transaction.find({ owner_id: req.query.id }, (err, response) => {
                if(response.length > 0) {
                    console.log("Response : ", response);
                    res.status(200).json(response);
                } else {
                    console.log("No response");
                    if(err){
                        console.log("ERROR : "+err);
                    }
                    res.sendStatus(400);
                }
            });
        }else{
            console.log("Missing id for list_userTxs");
            res.sendStatus(400);
        }
    }
    catch(error){
		console.log(error);
		res.status(400).send('Internal server error');
    }
};

exports.create_a_tx = function(req, res) {
	console.log(req.body)
	let Tx = new Transaction(req.body);
	Tx.save(function(err, Tx) {
		if (err) res.send(err);
		res.json(Tx);
		console.log('create_a_tx', Tx)
	});
};

exports.list_all_tx = function(req, res) {
	Transaction.find({}, function(err, Transaction) {
		if (err) res.status(500).send(err);
		res.json(Transaction);
		console.log('list_all_tx', Transaction);
	});
};

exports.list_some_tx = function(req, res) {
	Transaction.find({owner_id: req.params.ownerId}, function(err, Transaction) {
		if (err) res.send(err);
		res.json(Transaction);
		console.log('list_some_tx', Transaction)
	});
};