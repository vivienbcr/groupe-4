const Fund = require('../models/fundModel');

exports.init_fund = function(req, res) {
	let fundData = new Fund(req.body);
	fundData.save(function(err, fundData) {
		if (err) res.send(err);
		res.json(fundData);
		console.log('init_fund', fundData)
	});
};

exports.list_fund_data = function(req, res) {
	Fund.findOne({ name: 'groupe-4' }, function(err, Fund) {
		if (err) res.status(500).send(err);
		res.json(Fund);
		console.log('list_fund_data', Fund)
	});
};

exports.update_fund_data = function(req, res) {
		console.log('update_fund_data', req.body)
		Fund.findOne({ name: req.body.name }, function(err) {
			if (err) res.status(500).send(err);
			Fund.updateOne(
				{ name: req.body.name },
				{
					share_owned: req.body.share_owned
				}, function(err) {
				if (err) res.status(500).send(err);
				});
		});
};

