import axios from 'axios'
import jwt from 'jwt-decode'
const APIURL =
  process.env.NODE_ENV === 'development'
    ? 'http://localhost:3333/'
    : 'http://api:3000/'

// Utilisé uniquement pour les Déclaration des varribles du state
export const state = {
  currentUser: getSavedState('auth.currentUser'),
}
// Les mutations sont les modificateurs du state
export const mutations = {
  SET_CURRENT_USER(state, newValue) {
    state.currentUser = newValue
    saveState('auth.currentUser', newValue)
    setDefaultAuthHeaders(state)
  },
  UPDATE_CURRENT_USER(state, newValue) {
    state.currentUser.name = newValue.name
    state.currentUser.email = newValue.email
  },
}
export const getters = {
  // Whether the user is currently logged in.
  loggedIn(state) {
    return !!state.currentUser
  },
}
// Les actions préparent les données avant de les envoyer aux mutations
export const actions = {
  // This is automatically run in `src/state/store.js` when the app
  // starts, along with any other actions named `init` in other modules.
  init({ state, dispatch }) {
    setDefaultAuthHeaders(state)
    dispatch('validate')
  },

  // Logs in the current user.
  logIn({ commit, dispatch, getters }, { email, password } = {}) {
    if (getters.loggedIn) return dispatch('validate')
    // Requête d'authentification qui récupère un token
    return axios
      .post(APIURL + 'login/', JSON.stringify({ email, password }), {
        headers: {
          'Content-Type': 'application/json',
        },
      })
      .then((response) => {
        // extraction des informations du token
        const jwtdecode = jwt(response.data.token)
        const user = {
          token: response.data.token,
          name: jwtdecode.user.name,
          id: jwtdecode.user._id,
          email: jwtdecode.user.email,
          signin: jwtdecode.user.signin,
        }
        // Définis dans le store (vuex) un objet user qui est accessible partout dans l'application a partir du momment où l'utilisateur est connecté
        commit('SET_CURRENT_USER', user)
        return user
      })
      .catch((error) => {
        console.log('axios error ', error)
        return error
      })
  },
  /*
  Permet de modifier les informations de l'utilisateur dans le store
  */
  updateUser({ commit }, { name, email } = {}) {
    const nvalue = {
      name: name,
      email: email,
    }
    commit('UPDATE_CURRENT_USER', nvalue)
  },

  // Logs out the current user.
  /*
  Détruit le store contenant les informations de l'utilisateur
   */
  logOut({ commit }) {
    commit('SET_CURRENT_USER', null)
  },

  // Validates the current user's token and refreshes it
  // with new data from the API.
  /*
  Permet de vérrifier que le token de l'utilisateur est valide ou qu'il est toujours connecté (je n'ai pas utilisé la feature pour le momment)
  */
  validate({ commit, state }) {
    if (!state.currentUser) return Promise.resolve(null)
    // TODO VALIDATE USER
    // return axios
    //   .get('/api/session')
    //   .then((response) => {
    //     console.log('valid')
    //     const user = response.data
    //     commit('SET_CURRENT_USER', user)
    //     return user
    //   })
    //   .catch((error) => {
    //     console.log('nope')
    //     if (error.response && error.response.status === 401) {
    //       commit('SET_CURRENT_USER', null)
    //     }
    //     return null
    //   })
    return true
  },
}

// ===
// Private helpers
// ===

function getSavedState(key) {
  return JSON.parse(window.localStorage.getItem(key))
}

function saveState(key, state) {
  window.localStorage.setItem(key, JSON.stringify(state))
}
/*
Définis un header contenant le token d'authentification de l'utilisateur  dans toutes les requêtes axios de l'application
*/

function setDefaultAuthHeaders(state) {
  axios.defaults.headers.common.Authorization = state.currentUser
    ? state.currentUser.token
    : ''
}
