import Dashboard from './Dashboard'

describe('@views/Dashboard', () => {
  it('is a valid view', () => {
    expect(Dashboard).toBeAViewComponent()
  })
})
