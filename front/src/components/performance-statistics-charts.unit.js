import PerformanceStatisticsCharts from './performance-statistics-charts'

describe('@components/performance-statistics-charts', () => {
  it('exports a valid component', () => {
    expect(PerformanceStatisticsCharts).toBeAComponent()
  })
})
