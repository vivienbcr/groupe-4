import BuyToken from './buy-token'

describe('@components/buy-token', () => {
  it('exports a valid component', () => {
    expect(BuyToken).toBeAComponent()
  })
})
